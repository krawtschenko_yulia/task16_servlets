import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/pizzas/order")
public class PizzaOrderFormServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws IOException {
        res.setContentType("text/html;charset=UTF-8");

        HtmlElement html = new HtmlElement("html");

        HtmlElement head = new HtmlElement("head",
                new HtmlElement("title", "Pizza Paradise - Order Pizza"));
        HtmlElement meta = new HtmlElement("meta");
        meta.setAttribute("http-equiv", "Content-Type");
        meta.setAttribute("content", "text/html");
        meta.setAttribute("charset", "UTF-8");
        head.addHtmlElement(meta);

        HtmlElement body = new HtmlElement("body");
        HtmlElement form = new HtmlElement("form");
        form.setAttribute("method", "get");
        form.setAttribute("action", "echo");

        HtmlElement pizzaOptions = new HtmlElement("fieldset");
        pizzaOptions.addHtmlElement(new HtmlElement("legend", "Choose your pizza"));
        HtmlElement pizzaSelect = new HtmlElement("select");
        HtmlElement homemadeOption = new HtmlElement("option", "Homemade Pizza");
        homemadeOption.setAttribute("name", "pizza");
        homemadeOption.setAttribute("value", "homemade");
        pizzaSelect.addHtmlElement(homemadeOption);
        pizzaOptions.addHtmlElement(pizzaSelect);
        form.addHtmlElement(pizzaOptions);

        HtmlElement changeCrust = new HtmlElement("fieldset");
        changeCrust.addHtmlElement(new HtmlElement("legend", "What kind of crust do you like?"));
        HtmlElement crustChoiceThin = new HtmlElement("input");
        crustChoiceThin.setAttribute("type", "radio");
        crustChoiceThin.setAttribute("default", "true");
        crustChoiceThin.setAttribute("name", "crust");
        crustChoiceThin.setAttribute("value", "thin");
        crustChoiceThin.setTextContents("Thin");
        HtmlElement crustChoiceThick = new HtmlElement("input");
        crustChoiceThick.setAttribute("type", "radio");
        crustChoiceThick.setAttribute("name", "crust");
        crustChoiceThick.setAttribute("value", "thick");
        crustChoiceThick.setTextContents("Thick");
        changeCrust.addHtmlElement(crustChoiceThin);
        changeCrust.addHtmlElement(crustChoiceThick);
        form.addHtmlElement(changeCrust);

        HtmlElement chooseExtras = new HtmlElement("fieldset");
        chooseExtras.addHtmlElement(new HtmlElement("legend", "Would you like to add something?"));
        HtmlElement extrasOliveOil = new HtmlElement("input");
        extrasOliveOil.setAttribute("type", "checkbox");
        extrasOliveOil.setAttribute("name", "extras");
        extrasOliveOil.setAttribute("value", "olive oil");
        extrasOliveOil.setAttribute("checked", "true");
        extrasOliveOil.setTextContents("Olive Oil");
        chooseExtras.addHtmlElement(extrasOliveOil);
        form.addHtmlElement(chooseExtras);

        HtmlElement instructions = new HtmlElement("fieldset");
        instructions.addHtmlElement(new HtmlElement("legend", "Any additional instructions?"));
        HtmlElement textArea = new HtmlElement("textarea", "Write a message here");
        textArea.setAttribute("name", "instructions");
        textArea.setAttribute("rows", "10");
        textArea.setAttribute("cols", "30");
        instructions.addHtmlElement(textArea);
        form.addHtmlElement(instructions);

        HtmlElement submit = new HtmlElement("input", "Place Order");
        submit.setAttribute("type", "submit");
        submit.setAttribute("value", "Send");
        form.addHtmlElement(submit);
        body.addHtmlElement(form);

        html.addHtmlElement(head);
        html.addHtmlElement(body);

        try (PrintWriter out = res.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.print(html);
        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws IOException {
        res.sendRedirect("/pizzas/order/success");
    }
}
