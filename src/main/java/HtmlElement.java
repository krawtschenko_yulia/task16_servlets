import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HtmlElement {
    private String tag;
    private String textContents;
    private List<HtmlElement> htmlContents;

    private Map<String, String> attributes;

    public HtmlElement(String tag) {
        this.tag = tag;
    }

    public HtmlElement(String htmlTag, String text) {
        tag = htmlTag;
        textContents = text;
    }

    public HtmlElement(String htmlTag, List<HtmlElement> htmlElements) {
        tag = htmlTag;
        htmlContents = htmlElements;
    }

    public HtmlElement(String htmlTag, HtmlElement htmlElement) {
        tag = htmlTag;
        htmlContents = new ArrayList<>();
        htmlContents.add(htmlElement);
    }

    public void appendTextContents(String text) {
        textContents += text;
    }

    public String getTextContents() {
        return textContents;
    }

    public void setTextContents(String text) {
        textContents = text;
    }

    public List<HtmlElement> getHtmlContents() {
        return htmlContents;
    }

    public void setHtmlContents(List<HtmlElement> htmlElements) {
        htmlContents = htmlElements;
    }

    public void addHtmlElement(HtmlElement htmlElement) {
        if (htmlContents == null) {
            htmlContents = new ArrayList<>();
        }
        htmlContents.add(htmlElement);
    }

    public void setAttribute(String name, String value) {
        if (attributes == null) {
            attributes = new HashMap<>();
        }

        attributes.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder serialised = new StringBuilder();

        serialised.append("<");
        if (attributes != null) {
            for (String key : attributes.keySet()) {
                serialised.append(" " + key + "=\"" + attributes.get(key) + "\"");
            }
        }
        serialised.append(tag + ">");
        if (textContents != null) {
            serialised.append(textContents);
        } else if (htmlContents != null){
            for (HtmlElement inner : htmlContents) {
                serialised.append(inner.toString());
            }
        }
        serialised.append("</" + tag + ">");

        return serialised.toString();
    }
}
