import sun.font.CharToGlyphMapper;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/pizzas/order/success")
public class PizzaOrderSubmitServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setContentType("text/html; charset=\"UTF-8\"");

        HtmlPage page = new HtmlPage("Order Placed");

        HtmlElement header = new HtmlElement("h2", "Success!");
        page.addToBody(header);
        HtmlElement message = new HtmlElement("p", "Your pizza is on its way.");
        page.addToBody(message);
        HtmlElement recap = new HtmlElement("div");
        recap.addHtmlElement(new HtmlElement("p", "Here is what you ordered:"));
        recap.addHtmlElement(new HtmlElement("br"));

        String pizzaName = req.getParameter("pizza");
        recap.addHtmlElement(new HtmlElement("p", pizzaName + " Pizza"));
        recap.addHtmlElement(new HtmlElement("br"));

        String crust = req.getParameter("crust");
        recap.addHtmlElement(new HtmlElement("p", crust + " Crust"));
        recap.addHtmlElement(new HtmlElement("br"));

        String[] extras = req.getParameterValues("extras");
        recap.addHtmlElement(new HtmlElement("p", "Extras:"));
        HtmlElement extrasList = new HtmlElement("ul");
        for (String extra : extras) {
            extrasList.addHtmlElement(new HtmlElement("li", extra));
        }
        recap.addHtmlElement(extrasList);

        String instructions = req.getParameter("instructions");
        recap.addHtmlElement(new HtmlElement("p", instructions));

        try (PrintWriter out = res.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.print(page);
        }
    }
}
