package domain;

public enum OrderStatus {
    IN_PROGRESS,
    PLACED,
    ON_MAKE_LINE,
    IN_OVEN,
    READY_FOR_PICK_UP,
    ON_ITS_WAY,
    COMPLETE,
    CANCELLED
}
