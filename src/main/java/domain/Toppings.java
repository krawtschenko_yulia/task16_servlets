package domain;

public enum Toppings implements PizzaOption {
    TOMATOES("Tomatoes"),
    WILD_MUSHROOMS("Wild Mushrooms"),
    BUTTON_MUSHROOMS("Button Mushrooms"),
    TOFU("Tofu"),
    OLIVES("Olives"),
    ARTICHOKE_HEARTS("Artichoke Hearts"),
    FRENCH_PICKLES("French Pickles"),
    CARAMELISED_ONIONS("Caramelised Onions"),
    BELL_PEPPER("Bell Pepper"),
    SWEET_CORN("Sweet Corn"),
    CAPERS("Capers"),
    AUBERGINES("Aubergines"),
    COURGETTES("Courgettes"),
    BROCCOLI("Broccoli"),
    STRING_BEANS("String Beans"),
    PINEAPPLE("Pineapple");

    private final String name;

    Toppings(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
