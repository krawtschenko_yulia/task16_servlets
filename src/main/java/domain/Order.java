package domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Order {
    private Map<Pizza, Integer> pizzas;
    private OrderStatus status = OrderStatus.IN_PROGRESS;

    public Order() {
        pizzas = new HashMap<Pizza, Integer>();
    }

    public Order(Pizza pizza, Integer quantity) {
        pizzas = new HashMap<Pizza, Integer>();
        pizzas.put(pizza, quantity);
    }

    public Order(Pizza pizza) {
        new Order(pizza, 1);
    }

    public Order(Map<Pizza, Integer> pizzas) {
        pizzas = new HashMap<Pizza, Integer>(pizzas);
    }

    public Set<Pizza> getPizzaTypes() {
        return pizzas.keySet();
    }

    public void addPizzas(Pizza pizza, Integer quantity) {
        if (isModifiable()) {
            pizzas.put(pizza, quantity);
        }
    }

    public void addPizza(Pizza pizza) {
        if (isModifiable()) {
            addPizzas(pizza, 1);
        }
    }

    public void removePizza(Pizza pizza) {
        if(isModifiable()) {
            if (isOrdered(pizza)) {
                pizzas.remove(pizza);
            }
        }
    }

    public void plusOne(Pizza pizza) {
        if (isModifiable()) {
            if (isOrdered(pizza)) {
                pizzas.replace(pizza, pizzas.get(pizza) + 1);
            } else {
                addPizzas(pizza, 1);
            }
        }
    }

    public void minusOne(Pizza pizza) {
        if (isModifiable()) {
            if (isOrdered(pizza)) {
                int quantity = pizzas.get(pizza) - 1;

                if (quantity > 0) {
                    pizzas.replace(pizza, quantity);
                } else {
                    removePizza(pizza);
                }
            }
        }
    }

    public void changeStatus(OrderStatus status) {
        if (isCurrent()) {
            this.status = status;
        }
    }

    public void cancel() {
        changeStatus(OrderStatus.CANCELLED);
    }


    private boolean isOrdered(Pizza pizza) {
        return pizzas != null && pizzas.containsKey(pizza);
    }

    private boolean isModifiable() {
        return status == OrderStatus.IN_PROGRESS;
    }

    private boolean isCurrent() {
        return status != OrderStatus.COMPLETE
                && status != OrderStatus.CANCELLED;
    }
}
