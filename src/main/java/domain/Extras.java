package domain;

public enum Extras implements PizzaOption {
    OLIVE_OIL("Olive Oil"),
    GARLIC_OIL("Garlic Oil"),
    ROCKET_SALAD("Rocket Salad"),
    SPINACH("Spinach"),
    LETTUCE("Lettuce"),
    LEEKS("Leeks");

    private String name;

    Extras(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
