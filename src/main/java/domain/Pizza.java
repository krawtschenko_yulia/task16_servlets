package domain;

import java.util.List;

public class Pizza {
    private CrustOptions crust;
    private List<Toppings> toppings;
    private List<Extras> extras;

    private String name;

    public Pizza(CrustOptions crust,
                 List<Toppings> toppings,
                 List<Extras> extras,
                 String name) {
        this.crust = crust;
        this.toppings = toppings;
        this.extras = extras;
        this.name = name;
    }
/*
    public Pizza(CrustOptions crust, List<Toppings> toppings) {
        new Pizza(crust, toppings, null);
    }

 */

    public CrustOptions getCrustOption() {
        return crust;
    }

    public List<Toppings> getToppings() {
        return toppings;
    }

    public List<Extras> getExtras() {
        return extras;
    }

    public String getName() {
        return name;
    }
}
