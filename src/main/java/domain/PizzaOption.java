package domain;

public interface PizzaOption {
    public String getName();
}
