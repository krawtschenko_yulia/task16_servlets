package domain;

public enum CrustOptions implements PizzaOption {
    THIN("Thin"),
    THICK("Thick"),
    NO_YEAST("No-Yeast"),
    GLUTEN_FREE("Gluten-Free");

    private String name;

    CrustOptions(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
