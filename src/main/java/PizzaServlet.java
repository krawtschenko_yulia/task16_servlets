import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import domain.*;

@WebServlet("/pizzas")
public class PizzaServlet extends HttpServlet {
    private static List<Order> orders;
    private static Pizza specialty;

    @Override
    public void init() {
        List<Toppings> toppings = new ArrayList<>();
        toppings.add(Toppings.OLIVES);
        toppings.add(Toppings.TOFU);
        toppings.add(Toppings.TOMATOES);
        toppings.add(Toppings.WILD_MUSHROOMS);

        List<Extras> extras = new ArrayList<>();
        extras.add(Extras.OLIVE_OIL);

        Pizza homeRecipePizza = new Pizza(CrustOptions.THIN, toppings, extras,
                "Homemade");

        Order pizzaOrder = new Order(homeRecipePizza);

        specialty = homeRecipePizza;

    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws IOException {
        res.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = res.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<meta http-equiv='Content-Type' content='text/html; " +
                    "charset=UTF-8'>");
            out.println("<title>Pizza Paradise</title>");
            out.println("</head>");
            out.println("<body>");

            out.println("<h1>Today's Specialty</h1>");
            out.println("<h2>" + specialty.getName() + "</h2>");

            out.println("<p> Crust: " + CrustOptions.NO_YEAST.getName() + "</p>");

            out.println("<p> Toppings: </p>");
            out.println("<ul>");
            specialty.getToppings().forEach(topping ->
                    out.println("<li>" + topping.getName() + "</li>"));
            out.println("</ul>");

            out.println("<p>Extras:</p>");
            out.println("<ul>");
            specialty.getExtras().forEach(extra ->
                    out.println("<li>" + extra.getName() + "</li>"));
            out.println("</ul>");

            out.println("</body>");
            out.println("</html>");
        }
    }
}
