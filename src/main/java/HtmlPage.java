public class HtmlPage {
    private HtmlElement page;

    public HtmlPage(String title) {
        page = new HtmlElement("html");

        HtmlElement head = new HtmlElement("head",
                new HtmlElement("title", "Pizza Paradise - Order Pizza"));
        HtmlElement meta = new HtmlElement("meta");
        meta.setAttribute("http-equiv", "Content-Type");
        meta.setAttribute("content", "text/html");
        meta.setAttribute("charset", "UTF-8");
        head.addHtmlElement(meta);

        page.addHtmlElement(head);
        page.addHtmlElement(new HtmlElement("body"));
    }

    public void addToHead(HtmlElement element) {
        page.getHtmlContents().get(0).addHtmlElement(element);
    }

    public void addToBody(HtmlElement element) {
        page.getHtmlContents().get(1).addHtmlElement(element);
    }

    @Override
    public String toString() {
        return page.toString();
    }
}
